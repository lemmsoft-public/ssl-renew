# 1.1.1 / 26.08.2021
- NGINX docker container restart command fix.

# 1.1.0 / 26.08.2021
- Added support for multiple config paths.
- Reworked the dist/bin folder versioning scheme and the install script.

# 1.0.1 / 26.08.2021
- Switched over from exec to spawn and added real-time console output from the child processes.
- Added an install script.


# 1.0.0 / 26.08.2021
Initial version.