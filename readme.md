# SSL-RENEW
SSL Renew - a tool for renewing ssl certificates on NGINX webservers.<br/>
The goal of this program is to quickly process NGINX config files, prepare them for SSL certificate renewal, renew the certificates in question and restore the original config files' state.

The program reads an NGINX `.conf` file or a directory containing NGINX `.conf` files recursively and edits them according to rules defined by a particular pattern of comments, as follows:<br/>
`# sslRenew command numberOfLines`<br/>
The value of `command` can either be `disable` or `enable`. Base on that, the next `numberOfLines` line of the `.conf` file will be either disabled using a comment or enabled by removing the comment (`#`) symbol at the start of the line.

The program's execution is determined by the following list of switches:

| Switch | Required | Description |
| - | - | - |
| c, configPath | Yes | The path to the file or folder containing the .conf files. |
| n, nginxRestartScript | No | The path to the script file for restarting NGINX. If neither this, nor dockerContainer are provided, the program will run "systemctl restart nginx". |
| d, dockerContainer | No | The name of the NGINX docker container. If neither this, nor nginxRestartScript are provided, the program will run "systemctl restart docker". |
| s, sslRenewScriptPath | Yes | The path to the script file for renewing the SSL certificate. |
| h, help | No | Print this help. |

Note: the first of the above switches should be prefaced with a "--" to ensure node does not capture them and passess them to the program instead.
