import { argv } from 'yargs'
import childProcess from 'child_process'
import * as fs from 'fs'
import marked from 'marked'
import * as path from 'path'
import { promisify } from 'util'
import TerminalRenderer from 'marked-terminal'

marked.setOptions({ renderer: new TerminalRenderer() })

const commentRegex = new RegExp(/^(\s+|\t+)?#/)
const fsPromise = {
  close: promisify(fs.close),
  lstat: promisify(fs.lstat),
  open: promisify(fs.open),
  readdir: promisify(fs.readdir),
  readFile: promisify(fs.readFile),
  writeFile: promisify(fs.writeFile)
}
const spawn = function(command: string, args: string[] = []): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    const proc = childProcess.spawn(command, args)
    let error: string = ''
    proc.stdout.on('data', (data) => console.log(data.toString()))
    proc.stderr.on(
      'data',
      (data) => {
        const stringData = data.toString()
        console.error(stringData)
        error += stringData
      }
    )
    proc.on('close', (code) => code === 0 ? resolve() : reject(new Error(error)))
  })
}


const processConfig = async function(
  configPath: string,
  options: { initial: boolean }
): Promise<void> {
  const { initial } = options
  const pathData = await fsPromise.lstat(configPath)
  if (pathData.isDirectory()) {
    const dirContents = await fsPromise.readdir(configPath)
    for (const i in dirContents) {
      await processConfig(path.join(configPath, dirContents[i]), options)
    }
    return
  }
  if (!configPath.match(/\.conf$/)) {
    return
  }
  const fileData = (await fsPromise.readFile(configPath)).toString().split('\n')
  let newFileData: string[] = []
  let disableActive = false
  let enableActive = false
  let rowsForProcessingCount = -1
  fileData.forEach((row) => {
    const match = row.match(/#(\s+)?sslRenew\s(enable|disable)\s(\d+)/)
    if (match) {
      rowsForProcessingCount = parseInt(match[3])
      if (match[2] === 'enable') {
        enableActive = true
      }
      else if (match[2] === 'disable') {
        disableActive = true
      }
      newFileData.push(row)
      return
    }
    if (rowsForProcessingCount === 0) {
      rowsForProcessingCount = -1
      enableActive = false
      disableActive = false
    }
    if (rowsForProcessingCount === -1) {
      newFileData.push(row)
      return
    }
    if (enableActive) {
      if (initial) {
        newFileData.push(row.replace(commentRegex, '$1'))
      }
      else {
        newFileData.push(row.match(commentRegex) ? row : `#${row}`)
      }
      rowsForProcessingCount--
      return
    }
    if (disableActive) {
      if (initial) {
        newFileData.push(row.match(commentRegex) ? row : `#${row}`)
      }
      else {
        newFileData.push(row.replace(commentRegex, '$1'))
      }
      rowsForProcessingCount--
      return
    }
    newFileData.push(row)
  })
  await fsPromise.writeFile(configPath, newFileData.join('\n'))
}

const restartNGINX = async function(options: { dockerContainer?: string; scriptPath?: string } = {}): Promise<void> {
  console.log('[ssh-renew]: Restarting NGINX...')
  const { dockerContainer, scriptPath } = options
  let actualScriptPath = scriptPath
  if (dockerContainer) {
    return await spawn('docker', ['container', 'restart', dockerContainer])
  }
  if (!scriptPath) {
    try {
      return await spawn('systemctl', ['restart', 'nginx'])
    }
    catch(e) {
      if (e && e.message && (e.message.indexOf('nginx.service failed') !== -1)) {
        throw e
      }
      // console.error('[ssl-renew]:', e)
      console.log('[ssl-renew]: Defaulting to process.cwd()/restartNGINX.sh.')
      actualScriptPath = path.join(process.cwd(), 'restartNGINX.sh')
    }
  }
  return await spawn(actualScriptPath!)
}
;(async function() {
  if (argv.help || argv.h) {
    const readme = (await fsPromise.readFile(path.join(__dirname, '../../readme.md'))).toString()
    console.log(marked(readme))
    return
  }
  const configPath = argv.configPath || argv.c
  let configPaths: string[] = []
  if (configPath instanceof Array) {
    configPaths = configPath.map((item) => path.resolve(item))
  }
  else {
    configPaths.push(path.resolve(configPath as string))
  }
  const nginxRestartScriptPath = (argv.nginxRestartScript || argv.n) as string
  const restartNGINXOptions = {
    dockerContainer: (argv.dockerContainer || argv.d) as string,
    scriptPath: nginxRestartScriptPath ? path.resolve(nginxRestartScriptPath) : undefined
  }
  const argvSSLRenewScriptPath = (argv.sslRenewScript || argv.s) as string
  const sslRenewScriptPath =
    argvSSLRenewScriptPath ?
      path.resolve(argvSSLRenewScriptPath) :
      path.join(process.cwd(), 'sslRenew.sh')
  for (const i in configPaths) {
    await processConfig(configPaths[i], { initial: true })
  }
  await restartNGINX(restartNGINXOptions)
  console.log('[ssl-renew]: Generating SSL certificate...')
  await spawn(sslRenewScriptPath)
  for (const i in configPaths) {
    await processConfig(configPaths[i], { initial: false })
  }
  await restartNGINX(restartNGINXOptions)
  console.log('[ssl-renew]: Script completed.')
})().then(
  () => process.exit(0),
  (err) => {
    console.error(err)
    process.exit(1)
  }
)
